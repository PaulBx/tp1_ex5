# Exercice 5

## Ajout par rapport à l'exercice 4
Par rapport à l'exercice 4, on a changé les méthodes de la classe SimpleCalculator afin de permettre le contrôle de la nature des arguments notamment.


## Organisation
semblable à l'organisation de l'exercice 4

## Lancement
Pour éxecuter le script, il faut suivre les mêmes étapes que pour l'exercice 4,il faut juste se placer dans le répertoire de l'exercice 5 et remplacer la ligne:  
  
    pyhton Package_Test/exo_4.py

par:  

    python Package_Test/exo_5.py


## Réference vers l'exercice 4:
Vous pourrez trouver le dépôt gitlab de l'exercice 4 suivant ce lien :  
(https://gitlab.com/PaulBx/tp1_ex4)
