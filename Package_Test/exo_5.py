#!/usr/bin/env python
# coding : utf-8
"""
author : paul bouyssoux

-Ce script permet de tester les méthodes de la classe SimpleCalculator
comprise dans le package Package_Calculator

"""

#importation de la classe via le package
from Package_Calculator.Calculator import SimpleCalculator


#tests
MON_TEST = SimpleCalculator()
print("somme : 10 + 5 = ", MON_TEST.fsum(10,5))
print("somme : 10 + '5' = ", MON_TEST.fsum(10,"5"))
print("soustraction 10 - 5 = ", MON_TEST.substract(10,5))
print("soustraction 10 - '5' = ", MON_TEST.substract(10,"5"))
print("multiplication : 10 x 5 = ", MON_TEST.multiply(10,5))
print("multiplication : 10 x '5' = ", MON_TEST.multiply(10,"5"))
print("division : 10 / 5 = ", MON_TEST.divide(10,5))
print("division : 10 / 0 = ", MON_TEST.divide(10,0))
print("division : 10 / '5' = ",MON_TEST.divide(10,"5"))

